const express = require("express");
const router = express.Router();
const connection = require("../connection");

router.get("/", (req, res) => {
    connection.query("SELECT * from employeedetails", (err, rows, fields) => {
        if (!err) {
            res.send(rows);
        }
        else {
            console.log(err);
        }
    })

});

router.get("/:id", (req, res) => {
    connection.query('SELECT * FROM employeedetails WHERE empId = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

router.delete("/:id", (req, res) => {
    connection.query('DELETE FROM employeedetails WHERE empId = ?', [req.params.id], (err, rows, fields) => {
        if (!err)
            res.send(rows);
        else
            console.log(err);
    })
});

router.post('/', (req, res) => {
    var sql = "INSERT INTO employeedetails (empName,empPosition,empPhoneNumber,empAddress) VALUES ('"+req.body.empName+"','"+req.body.empPosition+"',"+req.body.empPhoneNumber+",'"+req.body.empAddress+"')";
    connection.query(sql, (err, rows, fields) => {
        if (!err) {
             res.send();
        }
        else {
            console.log(err);
        }
    });
});


router.put("/:id", (req, res) => {
    console.log(req.body.empName);
    console.log(req.params.id);
    var sql = "UPDATE employeedetails SET empName = '"+req.body.empName+"'  WHERE empId = ?";
    connection.query(sql,[req.params.id], (err, rows, fields) => {
        if (!err) {
             res.send(rows);
        }
        else {
            console.log(err);
        }
    });
});




module.exports = router;