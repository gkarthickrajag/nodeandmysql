const express = require("express");
const bodyParser = require("body-parser");
const employeeController = require("./controller/employeeController");
const connection = require("./connection");

var app = express();
app.use(bodyParser.json());

app.use("/employee",employeeController);

app.listen(3000);

